//Подключаем div#showMessage, в который будем вставлять сообщения
var showMessage = document.getElementById("showMessage");

//Подключаем p#message, который хранит по одному сообщению
var message = document.getElementById("message");



//Массив с готовыми ответами
var answers = [
	"Ты этого не увидишь никогда",
	"Да", 
	"Нет", 
	"Бойся 228", 
	"Верните мне мой 2007", 
	"Кавказ Сила, а Яжка могила", 
	"Покрути спинер и жизнь наладится"
];

//Массив для наших сообщений
var messages = [];

//Функция для отправки сообщения
function sendMessage(){

	//Получаем текст сообщения
	var text = document.getElementById("text");

	//Если поля ввода сообщения не пустое
	if(text.value != ""){

		//Получаем вводное сообщение
		var noMessages = document.getElementById("noMessages");
		//Убираем его
		noMessages.style.display = "none";

		//Добавляем новое сообщение в массив messages
		messages.push(text.value);
		//Добавляем новое сообщение на страницу
		showMessage.innerHTML += '<p id="message">Вы: '+ text.value +'</p>';
		//Получаем рандомный ответ из массива answers
		showMessage.innerHTML += '<p id="message">ЧатБот: '+ answers[Math.floor(Math.random() * (answers.length))] +'</p>';
 		//Очищаем поле ввода
		text.value = "";
	}
	//Если поле для ввода сообщение пустое
	else{
		alert("Вы не ввели ничего");
	}
}

//Событие нажатия на кнопку отправить
send.onclick = function() {
	//Вызываем функцию отправки сообщения
	sendMessage();
}

//События нажатия на кнопки на клавиатуре
text.onkeypress = function(event){
	//Если нажата кнопка enter
	if(event.keyCode==13){ 
		//Отменяем её стандартное действие (перенос на новую строку)
		event.preventDefault();
		//Вызываем функцию отправки сообщения
		sendMessage();
	}
}